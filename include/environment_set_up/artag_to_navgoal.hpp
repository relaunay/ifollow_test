#pragma once

#include <ros/ros.h>

class ArtagToNavGoal {

    public:

        ArtagToNavGoal(ros::NodeHandle & nd);
        ~ArtagToNavGoal() = default;
    private:
        void run();
        void load_goals();
        
        ros::NodeHandle nh_;
        ros::Publisher pub_;
        ros::Timer run_timer_;
        std::string paht_image;

        std::vector<geometry_msgs::PoseStamped>goal_list;
};