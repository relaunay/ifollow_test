#include "environment_set_up/artag_to_navgoal.hpp"
#include <ros/package.h>

#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>

#include<opencv2/opencv.hpp>

#include <geometry_msgs/PoseStamped.h>

ArtagToNavGoal::ArtagToNavGoal(ros::NodeHandle & nh):
nh_(nh)
{
  // Subscrive to input video feed and publish output video feed
    pub_ = nh.advertise<geometry_msgs::PoseStamped>("/cmd_local", 10);

    if(!nh.getParam("paht_image", paht_image))
    {
        ROS_ERROR("paht_image not found");
    }
    load_goals();
    run();
    ROS_INFO("artag to twist started");
}

void ArtagToNavGoal::load_goals()
{
    // todo
    return;
}

void ArtagToNavGoal::run()
{
    std::vector<cv::String> fn;
    cv::glob("~/data/*.png",fn,false);

    cv::aruco::Dictionary dictionary;
    dictionary.markerSize = 6;
    dictionary.maxCorrectionBits = 3;

    for(auto path : fn){
        auto path_crop = path.substr(path.find_last_of("/")+1,path.find(".png")-path.find_last_of("/")-1);
        cv::Mat image = imread(path, cv::IMREAD_COLOR );
        if (image.empty()) {
            std::cout << "Image File "
                    << "Not Found" << std::endl;
        }
        else{
            cv::resize(image, image, cv::Size(10,10),0, 128, cv::INTER_AREA);
            image = image(cv::Range(2,8),cv::Range(2,8));
            cv::cvtColor( image, image, cv::COLOR_BGR2GRAY);
            cv::threshold(image,image,100,1,cv::THRESH_BINARY);
            cv::Mat markerCompressed = cv::aruco::Dictionary::getByteListFromBits(image);
        // add the marker as a new row
        dictionary.bytesList.push_back(markerCompressed);
        }
    }
    cv::Mat image_test = cv::imread(paht_image);
    cv::Ptr<cv::aruco::Dictionary> dictionary_const = &dictionary;
    cv::Mat imageCopy;
    image_test.copyTo(imageCopy);
    std::vector<int> ids;
    std::vector<std::vector<cv::Point2f> > corners;
    cv::resize(imageCopy, imageCopy,imageCopy.size()/4,cv::INTER_LINEAR);
    cv::aruco::detectMarkers(imageCopy, dictionary_const, corners, ids);

    if (ids.size() > 0){
        cv::aruco::drawDetectedMarkers(imageCopy, corners, ids);
        pub_.publish(goal_list[ids[0]]);
    }
}

