#include "environment_set_up/artag_to_navgoal.hpp"
#include "ros/ros.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "artag_to_twist");
  ros::NodeHandle node_handle("~");
  ArtagToNavGoal artag_to_nav_goal(node_handle);
  ros::spin();
  return 0;
}