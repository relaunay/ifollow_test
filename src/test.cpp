#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>

#include<opencv2/opencv.hpp>

// test ar tag ros less

int main(){

std::vector<cv::String> fn;

cv::glob("/home/launay/Bureau/_data/*.png",fn,false);
cv::aruco::Dictionary dictionary;
// markers of 6x6 bits
dictionary.markerSize = 6;
// maximum number of bit corrections
dictionary.maxCorrectionBits = 3;

for(auto path : fn){
    cv::Mat image = imread(path, cv::IMREAD_COLOR );
    if (image.empty()) {
        std::cout << "Image File "
                << "Not Found" << std::endl;
    }
    else{
        cv::resize(image, image, cv::Size(10,10),0, 128, cv::INTER_AREA);
        image = image(cv::Range(2,8),cv::Range(2,8));
        cv::cvtColor( image, image, cv::COLOR_BGR2GRAY);
        cv::threshold(image,image,100,1,cv::THRESH_BINARY);
        cv::Mat markerCompressed = cv::aruco::Dictionary::getByteListFromBits(image);
    // add the marker as a new row
    dictionary.bytesList.push_back(markerCompressed);
    }
}

cv::Mat image_test = cv::imread("/home/launay/Bureau/_data/ar_tag_2.JPG");

cv::Ptr<cv::aruco::Dictionary> dictionary_const = &dictionary;

cv::Mat imageCopy;
// inputVideo.retrieve(image);
image_test.copyTo(imageCopy);
std::vector<int> ids;
std::vector<std::vector<cv::Point2f> > corners;
cv::resize(imageCopy, imageCopy,imageCopy.size()/4,cv::INTER_LINEAR);
cv::aruco::detectMarkers(imageCopy, dictionary_const, corners, ids);

if (ids.size() > 0){
    cv::aruco::drawDetectedMarkers(imageCopy, corners, ids);    
}

while (true) {
    cv::imshow("out", imageCopy);
    char key = (char) cv::waitKey(1);
    if (key == 27)
        break;  
    }
return 0;
}
