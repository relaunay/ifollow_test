
Environment set up 
=======

#### 1 . Set up a test environement (45mins to debug my pc and plays with)

(and incidentally answer the first question of the Test recruitment deployment file - 2022.pdf)

- Install the good version of ROS desktop-full based on your Ubuntu version:
    - Ubuntu 18.04 -> ROS Melodic, [follow the site guide](http://wiki.ros.org/melodic/Installation/Ubuntu)
    - Ubuntu 20.04 -> ROS Noetic , [follow the site guide](http://wiki.ros.org/noetic/Installation/Ubuntu)
    - ...

- Install turtlebot3_simulations 
```
    sudo apt-get install ros-${ROS_DISTRO}-turtlebot3 ros-${ROS_DISTRO}-turtlebot3-msgs
    sudo apt-get install ros-${ROS_DISTRO}-turtlebot3-simulations 
    sudo apt-get install ros-${ROS_DISTRO}-turtlebot3-navigation
    sudo apt-get install ros-${ROS_DISTRO}-turtlebot3-gazebo
    sudo apt-get install ros-${ROS_DISTRO}-dwa-local-planner 
```
- Launch simulation

I folow this [tutorial](https://emanual.robotis.com/docs/en/platform/turtlebot3/nav_simulation/) with one modification:

`roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml` become `roslaunch turtlebot3_navigation turtlebot3_navigation.launch` to use the default map world in turtlebot3-navigation

SO I lauch two terminal:
 - One the fisrt :

        export TURTLEBOT3_MODEL=burger
        roslaunch turtlebot3_gazebo turtlebot3_world.launch

 - The second terminal:

        export TURTLEBOT3_MODEL=burger
        roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml

After that on the rviz windows I set 2d pose estimate 

- Control bot byteleop (in a other teminal)
    - thiw touchs "w a d w s"
            
            roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch

    - somthing like this on terminal

            rostopic pub /cmd_vel geometry_msg/Twist  "{linear:  {x: 0.0, y: 0.0, z: 0.0}, angular: {x: 0.0,y: 0.0, z: 0.0}}

- Control bot by nav goal 2D:

    On rviz use 2D nav goal bouton on the top and after select your goal or `rqt` 
    with the message publisher to send message on topic _/move\_base/goal_

### 2. Multiplexeur commande (5min the time to fide the doc)

To add a node to multiplex _/cmd\_vel_ from _/cmd\_web_ & _/cmd\_local_ command on new terminal

    rosrun topic_tools mux /cmd_vel /cmd_web /cmd_local mux:=/mux_cmdvel

To swith topic source (ex with source /cmd_web)

    rosrun topic_tools mux_select mux_cmdvel /cmd_web

### 3. distance teleoparation (2h I have never use mqtt before)

I had to make a comte on a free mqtt broker to test

To launch rqtt teleop with out ros 

    ./scripts/pub_rqtt_rosless.py

To launch rqtt to cmd (twist msg)

    rosrun  environemment_set_up mqtt_to_twist.py

### 4. ar tag to cmd (1h to make the solotion ros less + 30 min to make the code in ros structure)

I havn't finish this part i don't have time.
The code is on _src_ and _includ_ and some on _config_.
I have the cpp base to read they ar tag. 
I have pased some time to good ar code in the dictionarie(1h)
finaly I have make a mehtode to get pdf file like model. 
But I need the images on png so I use this command for all pdf:

    pdftoppm ARtag<nb>.pdf ARta -png

So my code can read all model image in a folder make a dictionarie and find this ar tag in a images.(test.cpp)
I have somme problème to load the list of goal (I wanted to put all in yaml file on config).
It's because it's a list of geometry_msgs/PoseStamped messages.




### Authors
See the [AUTHORS](AUTHORS.md) file for a full list of contributors.