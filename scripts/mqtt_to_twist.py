#!/usr/bin/env python
import time
import paho.mqtt.client as paho
from paho import mqtt
from mqtt_cfg import *

import curses

import rospy
from geometry_msgs.msg import Twist

import ast
import io
import json

msg_str = str({"linear_vel": 0.0, "angular_vel": 0.0})
msg_is_resev = False

def cmd_publish(msg):
    msg_tmp = msg[2:len(msg)-1]
    msg_json = json.loads(msg_tmp)
    msg_to_send = Twist()
    msg_to_send.linear.x = msg_json['linear_vel']
    msg_to_send.angular.z = msg_json['angular_vel']
    pub.publish(msg_to_send)


# setting callbacks for different events to see if it works, print the message etc.
def on_connect(client, userdata, flags, rc, properties=None):
    print("CONNACK received with code %s." % rc)

# print which topic was subscribed to
def on_subscribe(client, userdata, mid, granted_qos, properties=None):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

# print message, useful for checking if it was successful
def on_message(client, userdata, msg):
    global msg_str 
    global msg_is_resev
    # print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    msg_str = str(msg.payload)
    # print(str(msg.payload))
    msg_is_resev = True


# Main function.
if __name__ == '__main__':
    # ros init
    try:
        rospy.init_node('mqtt_to_twist')
        pub = rospy.Publisher('/cmd_web', Twist, queue_size=10)
        rate = rospy.Rate(1)
        # mqtt init 
        client = paho.Client(client_id="", userdata=None, protocol=paho.MQTTv5)
        client.on_connect = on_connect

        # enable TLS for secure connection
        client.tls_set(tls_version=mqtt.client.ssl.PROTOCOL_TLS)
        # set username and password
        client.username_pw_set(USER_NAME, USER_PASSWORD)
        # connect to HiveMQ Cloud on port 8883 (default for MQTT)
        client.connect(MQTT_URL, MQTT_PORT)

        # setting callbacks, use separate functions like above for better visibility
        client.on_subscribe = on_subscribe
        client.on_message = on_message

        # subscribe to all topics of encyclopedia by using the wildcard "#"
        client.subscribe("ros/cmd_keybord", qos=1)

        client.loop_start()

        while not rospy.is_shutdown():
            if msg_is_resev:
                cmd_publish(msg_str)
                msg_is_resev = False
            rate.sleep()

        # client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()
        client.loop_stop()