#!/usr/bin/env python

import time
import paho.mqtt.client as paho
from paho import mqtt
from mqtt_cfg import *

import curses
import json

# setting callbacks for different events to see if it works, print the message etc.
def on_connect(client, userdata, flags, rc, properties=None):
    screen.addstr("CONNACK received with code %s." % rc)

# with this callback you can see if your publish was successful
def on_publish(client, userdata, mid, properties=None):
    screen.addstr("mid: " + str(mid))

# print message, useful for checking if it was successful
def on_message(client, userdata, msg):
    screen.addstr(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

client = paho.Client(client_id="", userdata=None, protocol=paho.MQTTv5)
client.on_connect = on_connect

# enable TLS for secure connection
client.tls_set(tls_version=mqtt.client.ssl.PROTOCOL_TLS)
# set username and password
client.username_pw_set(USER_NAME, USER_PASSWORD)
# connect to HiveMQ Cloud on port 8883 (default for MQTT)
client.connect(MQTT_URL, MQTT_PORT)

# setting callbacks, use separate functions like above for better visibility
client.on_message = on_message
client.on_publish = on_publish

# a single publish, this can also be done in loops, etc.

linear_vel, angular_vel = 0.0, 0.0

screen = curses.initscr()
try:
    curses.noecho()
    curses.curs_set(0)
    screen.keypad(1)
    screen.addstr("Press a arows to cotrol the bot and space to stop \n")
    screen.addstr("Ctrl + c to exit \n")
    while True:
        event = screen.getch()
        if event == curses.KEY_LEFT:
            angular_vel -=0.1
        elif event == curses.KEY_RIGHT:
            angular_vel +=0.1
        if event == curses.KEY_UP:
            linear_vel +=0.1
        elif event == curses.KEY_DOWN:
            linear_vel -=0.1
        elif event == 32:
            angular_vel = 0.0
            linear_vel = 0.0
        msg = {"linear_vel": linear_vel, "angular_vel": angular_vel}
        client.publish("ros/cmd_keybord", payload=json.dumps(msg), qos=1)
        screen.addstr("send : "+json.dumps(msg) + "\n")
finally:

    angular_vel = 0.0
    linear_vel = 0.0
    msg = {"linear_vel": linear_vel, "angular_vel": angular_vel}

    client.disconnect()
    client.loop_stop()
    curses.endwin()